class String
  def character_count

    digit_character_count, lowercase_character_count, uppercase_character_count, special_character_count = [0, 0, 0, 0]
    digit_character_range, lowercase_character_range, uppercase_character_range = ['0'..'9', 'a'..'z', 'A'..'Z']

    each_char do |character|
      case character
      when digit_character_range
        digit_character_count += 1
      when lowercase_character_range
        lowercase_character_count += 1
      when uppercase_character_range
        uppercase_character_count += 1
      else
        special_character_count += 1
      end
    end

    [digit_character_count, lowercase_character_count, uppercase_character_count, special_character_count]

  end
end

