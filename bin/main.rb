require_relative "../lib/string.rb"

print "Enter a Line : "
line = gets.chomp

digit_character_count, lowercase_character_count, uppercase_character_count, special_character_count = line.character_count
puts "No of Digits Character    : #{ digit_character_count }"
puts "No of Lowercase Character : #{ lowercase_character_count }"
puts "No of Uppercase Character : #{ uppercase_character_count }"
puts "No of Special Character   : #{ special_character_count }"

